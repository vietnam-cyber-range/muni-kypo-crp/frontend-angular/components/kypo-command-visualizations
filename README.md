# Training command graph visualization

## How to run locally with provided dummy data

1.  Run `npm install`.
2.  Run the server with `npm run api` or with provided parameters `json-server -w ./utils/json-server/db.js --routes ./utils/json-server/routes.json --middlewares ./utils/json-server/server.js`.
3.  Run the app in local environment and ssl `npm start` and access it on `https://localhost:4200`.
