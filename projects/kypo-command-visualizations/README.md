# Visualizations

## Mistake component
Has two types of views, one for trainee and one for organizer.

#### Organizer view
Organizer is able to filter between commands. Organizer also has possibility
to switch between wrong and correct commands. Another filter option is to
display only certain trainees.

#### Trainee view
Trainee can only see his commands and by default see every mistake type he/she has done.
### Usage
For Organizer view
```angular2html
<kypo-mistake></kypo-mistake>
```

For Trainee view
```angular2html
<kypo-mistake [sandboxId]="sandboxID"></kypo-mistake>
```


## Reference graph
Reference graph shows reference solution for training. Its zoomable component which allows traversal through graph.
### Usage
```angular2html
<kypo-reference-graph></kypo-reference-graph>
```


## Summary graph
Summary graph shows results of all trainees in training. Its zoomable component which allows traversal through graph.
### Usage
```angular2html
<kypo-summary-graph></kypo-summary-graph>
```


## Timeline
Command timeline displaying timeline for one trainee. Organizer is able to choose between displayed trainees.
Detail about particular commands is displayed along with time they were submitted.
### Usage
For Organizer view
```angular2html
<kypo-timeline></kypo-timeline>
```

For Trainee view
```angular2html
<kypo-timeline [sandboxId]="sandboxID"></kypo-timeline>
```


## Trainee graph
Trainee graph shows what commands trainee used, to which state they brought him/her
to. Its zoomable component which allows traversal through graph.
### Usage
```angular2html
<kypo-trainee-graph></kypo-trainee-graph>
```
