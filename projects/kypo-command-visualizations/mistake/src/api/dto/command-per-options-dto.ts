export class CommandPerOptionsDTO {
  cmd: string;
  cmd_type: string;
  options: string;
  mistake: string;
  from_host_ip: string;
  frequency: number;
}
