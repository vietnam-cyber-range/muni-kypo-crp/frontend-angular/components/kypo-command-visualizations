export class Participant {
  userRefId: number;
  sub: string;
  fullName: string;
  givenName: string;
  familyName: string;
  iss: string;
}
