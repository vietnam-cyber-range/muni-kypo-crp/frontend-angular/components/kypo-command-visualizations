export class CommandDTO {
  timestamp: string;
  training_time: string;
  from_host_ip: string;
  options: string;
  command_type: string;
  cmd: string;
}
