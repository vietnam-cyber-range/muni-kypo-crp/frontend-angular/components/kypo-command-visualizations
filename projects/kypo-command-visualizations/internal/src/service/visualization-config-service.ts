import { Injectable } from '@angular/core';
import { CommandVisualizationConfig } from '../model/command-visualization-config';

/**
 * Configuration service holding state of the provided config
 */
@Injectable()
export class VisualizationConfigService {
  readonly TIMELINE_EXTENSION: string = '';
  readonly COMMANDS_EXTENSION: string = '/all';

  /**
   * Config provided by client
   */
  config: CommandVisualizationConfig;

  constructor(config: CommandVisualizationConfig) {
    this.config = config;
  }
}
