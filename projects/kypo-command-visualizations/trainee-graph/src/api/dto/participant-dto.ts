export class ParticipantDTO {
  user_ref_id: number;
  sub: string;
  full_name: string;
  given_name: string;
  family_name: string;
  iss: string;
}
