# Trainee graph
Trainee graph shows what commands trainee used, to which state they brought him/her
to. Its zoomable component which allows traversal through graph.
## Usage
```angular2html
<kypo-trainee-graph [trainingInstanceId]="trainingInstanceId" [trainingRunId]="trainingRunId"></kypo-trainee-graph>
```
