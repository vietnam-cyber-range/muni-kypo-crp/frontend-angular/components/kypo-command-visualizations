# Reference graph
Reference graph shows reference solution for training. Its zoomable component which allows traversal through graph.
## Usage
```angular2html
<kypo-reference-graph [trainingInstanceId]="trainingInstanceId"></kypo-reference-graph>
```
