# Summary graph
Summary graph shows results of all trainees in training. Its zoomable component which allows traversal through graph.
## Usage
```angular2html
<kypo-summary-graph [trainingInstanceId]="trainingInstanceId"></kypo-summary-graph>
```
