import { Component, Input, OnInit } from '@angular/core';
import { Graphviz, graphviz } from 'd3-graphviz';
import { SummaryGraphService } from './service/summary-graph.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'kypo-summary-graph',
  templateUrl: './summary-graph.component.html',
  styleUrls: ['./summary-graph.component.css'],
})
export class SummaryGraphComponent implements OnInit {
  constructor(private graphService: SummaryGraphService) {}

  @Input() trainingInstanceId: number;
  private graphviz: Graphviz<any, any, any, any>;
  error = undefined;

  ngOnInit(): void {
    this.graphService
      .getSummaryGraph(this.trainingInstanceId)
      .pipe(
        tap(
          (result) => (this.graphviz = graphviz('div.graph').fit(true).renderDot(result.graph)),
          (err) =>
            (this.error =
              err.error.api_sub_error && err.error.status === 'NOT_FOUND'
                ? `The summary graph for training instance hasn't been created yet.`
                : 'Error occurred please refresh the page.')
        )
      )
      .subscribe();
  }

  onResetZoom(): void {
    this.graphviz.resetZoom();
  }
}
