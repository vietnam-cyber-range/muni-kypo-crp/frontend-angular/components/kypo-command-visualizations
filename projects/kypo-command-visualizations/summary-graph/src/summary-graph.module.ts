import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryGraphComponent } from './summary-graph.component';
import { SummaryGraphService } from './service/summary-graph.service';
import { MatButtonModule } from '@angular/material/button';
import { SummaryGraphApiService } from './api/summary-graph-api.service';
import { VisualizationConfigService } from '@muni-kypo-crp/command-visualizations/internal';
import { CommandVisualizationConfig } from '@muni-kypo-crp/command-visualizations/internal';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [SummaryGraphComponent],
  imports: [CommonModule, MatButtonModule, MatListModule, MatIconModule],
  exports: [SummaryGraphComponent],
  providers: [SummaryGraphService, SummaryGraphApiService, VisualizationConfigService],
})
export class SummaryGraphModule {
  static forRoot(config: CommandVisualizationConfig): ModuleWithProviders<SummaryGraphModule> {
    return {
      ngModule: SummaryGraphModule,
      providers: [{ provide: CommandVisualizationConfig, useValue: config }],
    };
  }
}
