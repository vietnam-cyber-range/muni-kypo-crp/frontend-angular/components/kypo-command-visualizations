import { Component, OnInit } from '@angular/core';
import { Agenda, AgendaContainer } from '@sentinel/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  agendaContainers: AgendaContainer[];

  ngOnInit(): void {
    const visualizationAgendas = [
      new Agenda('Mistake', 'mistake'),
      new Agenda('Reference Graph', 'reference-graph'),
      new Agenda('Summary Graph', 'summary-graph'),
      new Agenda('Trainee Graph', 'trainee-graph'),
      new Agenda('Timeline', 'timeline'),
    ];
    this.agendaContainers = [new AgendaContainer('Visualizations', visualizationAgendas)];
  }
}
