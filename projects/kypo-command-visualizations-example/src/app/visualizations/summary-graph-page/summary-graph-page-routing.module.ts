import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SummaryGraphPageComponent } from './summary-graph-page.component';

const routes: Routes = [
  {
    path: '',
    component: SummaryGraphPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SummaryGraphPageRoutingModule {}
