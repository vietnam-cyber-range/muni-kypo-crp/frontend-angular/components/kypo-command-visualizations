import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryGraphPageRoutingModule } from './summary-graph-page-routing.module';
import { SummaryGraphPageComponent } from './summary-graph-page.component';
import { SummaryGraphModule } from '@muni-kypo-crp/command-visualizations/summary-graph';
import { environment } from '../../../environments/environment';

@NgModule({
  declarations: [SummaryGraphPageComponent],
  imports: [
    CommonModule,
    SummaryGraphPageRoutingModule,
    SummaryGraphModule.forRoot(environment.commandVisualizationConfig),
  ],
})
export class SummaryGraphPageModule {}
