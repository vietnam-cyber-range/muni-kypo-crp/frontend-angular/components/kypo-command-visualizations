import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferenceGraphPageRoutingModule } from './reference-graph-page-routing.module';
import { ReferenceGraphPageComponent } from './reference-graph-page.component';
import { ReferenceGraphModule } from '@muni-kypo-crp/command-visualizations/reference-graph';
import { environment } from '../../../environments/environment';

@NgModule({
  declarations: [ReferenceGraphPageComponent],
  imports: [
    CommonModule,
    ReferenceGraphModule.forRoot(environment.commandVisualizationConfig),
    ReferenceGraphPageRoutingModule,
  ],
})
export class ReferenceGraphPageModule {}
