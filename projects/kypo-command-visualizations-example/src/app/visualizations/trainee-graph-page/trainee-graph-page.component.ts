import { Component } from '@angular/core';

@Component({
  selector: 'app-trainee-graph-page',
  templateUrl: './trainee-graph-page.component.html',
  styleUrls: ['./trainee-graph-page.component.css'],
})
export class TraineeGraphPageComponent {}
