import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TraineeGraphPageComponent } from './trainee-graph-page.component';
import { TraineeGraphPageRoutingModule } from './trainee-graph-page-routing.module';
import { TraineeGraphModule } from '@muni-kypo-crp/command-visualizations/trainee-graph';
import { environment } from '../../../environments/environment';

@NgModule({
  declarations: [TraineeGraphPageComponent],
  imports: [
    CommonModule,
    TraineeGraphPageRoutingModule,
    TraineeGraphModule.forRoot(environment.commandVisualizationConfig),
  ],
})
export class TraineeGraphPageModule {}
