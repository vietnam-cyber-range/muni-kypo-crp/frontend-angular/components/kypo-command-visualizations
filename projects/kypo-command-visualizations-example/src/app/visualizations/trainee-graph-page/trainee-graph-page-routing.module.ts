import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeGraphPageComponent } from './trainee-graph-page.component';

const routes: Routes = [
  {
    path: '',
    component: TraineeGraphPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TraineeGraphPageRoutingModule {}
