import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MistakePageComponent } from './mistake-page.component';

const routes: Routes = [
  {
    path: '',
    component: MistakePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MistakePageRoutingModule {}
