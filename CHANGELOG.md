### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* 987339a -- [CI/CD] Update packages.json version based on GitLab tag.
* af753fb -- Merge branch '9-update-to-angular-16' into 'master'
* 08f6dd2 -- Update local issuer to keycloak
* 45e5c70 -- Update to Angular 16
### 15.1.6 Fix commands not displaying.
* 9cf89b2 -- [CI/CD] Update packages.json version based on GitLab tag.
* 77e3f5d -- Merge branch 'forbidden-commands-integration' into 'master'
* 9e41a16 -- Forbidden commands integration
### 15.1.5 Update boolean directive of command.
* decc652 -- [CI/CD] Update packages.json version based on GitLab tag.
* eeab923 -- Merge branch 'forbidden-commands-integration' into 'master'
* 7bff18b -- Forbidden commands integration
### 15.1.4 Fix behavior subject for forbidden commands.
* 53fd1e4 -- [CI/CD] Update packages.json version based on GitLab tag.
* 685b5d9 -- Merge branch 'forbidden-commands-integration' into 'master'
* 044d10a -- Forbidden commands integration
### 15.1.3 Change background color for forbidden commands.
* 35b21d8 -- [CI/CD] Update packages.json version based on GitLab tag.
* 0955225 -- Merge branch 'forbidden-commands-integration' into 'master'
* cfb324d -- Merge branch 'master' into 'forbidden-commands-integration'
* 889bb05 -- fix version number
* af955e2 -- Change background color for forbidden commands
* 917dcb3 -- Merge branch 'master' into 'forbidden-commands-integration'
* eb178cc -- Update version
* 73211c9 -- Update detected forbidden commands model
* 8a72b78 -- Fix colorization issue
* c4b3e6e -- Update visualization of forbidden commands
* 468f7f6 -- Merge branch 'master' into forbidden-commands-integration
* 78c0742 -- Update version
* 8c60c15 -- Add color recognition for forbidden commands
* 1a5101d -- integrate forbidden commands with color
### 15.1.2 Update detected forbidden command model.
* 89ecc33 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5796c69 -- Merge branch 'forbidden-commands-integration' into 'master'
* 07bcabf -- Forbidden commands integration
### 15.1.1 Fix colorization issues.
* c9235f4 -- [CI/CD] Update packages.json version based on GitLab tag.
* e13a2f7 -- Merge branch 'forbidden-commands-integration' into 'master'
* d0cd737 -- Forbidden commands integration
### 15.1.0 Add support for cheating detection colorization of commands.
* 8eb4b53 -- [CI/CD] Update packages.json version based on GitLab tag.
* 0c28b79 -- Merge branch 'forbidden-commands-integration' into 'master'
* 7b5676e -- Forbidden commands integration
### 15.0.0 Update to Angular 15
* 1664878 -- [CI/CD] Update packages.json version based on GitLab tag.
* 1e5fe0e -- Merge branch '8-update-to-angular-15' into 'master'
* 43324cc -- Update to Angular 15
### 14.2.1 Fix command timeline encapsulation.
* d33d4dc -- [CI/CD] Update packages.json version based on GitLab tag.
* 30f6658 -- Merge branch 'fix-timeline-encapsulation' into 'master'
* 6b31961 -- Fix encapsulation of timeline component
### 14.2.0 Make command timeline more generic for use with multiple endpoints.
* bd3c883 -- [CI/CD] Update packages.json version based on GitLab tag.
* 111eba8 -- Merge branch '7-make-timeline-visualization-more-generic' into 'master'
* b94909f -- Resolve "Make timeline visualization more generic"
### 14.1.0 Remove elastic search service url from config
* dbfaa06 -- [CI/CD] Update packages.json version based on GitLab tag.
* df28b8a -- Merge branch '6-remove-elasticsearch-from-config' into 'master'
* 244f162 -- Resolve "Remove elasticSearch from config"
### 14.0.0 Update to Angular 14.
* 0a21b94 -- [CI/CD] Update packages.json version based on GitLab tag.
* 441de5d -- Merge branch '5-update-to-angular-14' into 'master'
* 3211361 -- Resolve "Update to Angular 14"
### 13.0.1 Add support to get reference graph by training definition ID.
* b08f100 -- [CI/CD] Update packages.json version based on GitLab tag.
* 5f15418 -- Merge branch '4-export-graph-model' into 'master'
* 94bc8cb -- Resolve "Export graph model"
### 13.0.0 Update to Angular 13 and CI/CD update
* 96d9c19 -- [CI/CD] Update packages.json version based on GitLab tag.
* 7223b0f -- Merge branch '3-update-to-angular-13' into 'master'
* 7d2336e -- Resolve "Update to Angular 13"
### 12.0.1 Command analysis visualizations
